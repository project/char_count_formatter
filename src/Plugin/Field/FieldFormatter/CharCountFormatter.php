<?php

namespace Drupal\char_count_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'char_count' formatter.
 *
 * @FieldFormatter(
 *   id = "char_count",
 *   label = @Translation("Character count formatter"),
 *   module = "char_count_formatter",
 *   field_types = {
 *     "string_long",
 *     "string",
 *     "text_long",
 *     "text",
 *   }
 * )
 */
class CharCountFormatter extends FormatterBase {

  /**
   *  {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Declare a setting named 'text_length', with
      // a default value of 'short'
      'text_css_class_below_bp' => 'below',
      'text_length' => 50,
      'text_css_class_above_bp' => 'above',
    ] + parent::defaultSettings();
  }
  
  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Conditionally assign CSS classes based on the character count of a text field.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    $threshold = $this->getSetting('text_length');

    foreach ($items as $delta => $item) {
      // Get the length of the string.
      $stringLength = strlen($item->value);

      // Determine which CSS class to apply based threshold setting.
      $cssClass = $stringLength <= $threshold ? $this->getSetting('text_css_class_below_bp') : $this->getSetting('text_css_class_above_bp');

      // Set the theme and then render array keys to pass the correct css class.
      // The CSS class will be mapped to the theme variables class property.
      $element[$delta] = [
        '#theme' => 'char_count_formatter',
        '#value' => $item->value,
        '#class' => $cssClass,
      ];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['text_css_class_below_bp'] = [
      '#title' => $this->t('CSS class below threshold.'),
      '#description' => $this->t("CSS class to be added when there are less than {$this->getSetting('text_length')} characters."),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('text_css_class_below_bp'),
    ];

    $form['text_length'] = [
      '#title' => $this->t('Text length'),
      '#description' => $this->t('Character count threshold.'),
      '#type' => 'number',
      '#min' => 1,
      '#default_value' => $this->getSetting('text_length'),
    ];

    $form['text_css_class_above_bp'] = [
      '#title' => $this->t("CSS class above threshold"),
      '#description' => $this->t("CSS class to be added when there are more than {$this->getSetting('text_length')} characters."),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('text_css_class_above_bp'),
    ];

    return $form;
  }

}